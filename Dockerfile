FROM ruby:2.5
MAINTAINER Michał "rysiek" Woźniak <rysiek@occrp.org>
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y \
    libmagic-dev \
    libgpgme11-dev \
    wget \
    git \
    git-core \
    inotify-tools \
    --no-install-recommends && rm -rf /var/lib/apt/lists/*

# get and install schleuder
# applying the unlocalhost patch in the process
RUN git clone https://0xacab.org/schleuder/schleuder.git /opt/schleuder && \
    cd /opt/schleuder && \
    bundle install

# get and install schleuder-cli
# not required, but helpful for CLI-based list administration
RUN git clone https://0xacab.org/schleuder/schleuder-cli.git /opt/schleuder-cli && \
    cd /opt/schleuder-cli && \
    bundle install && \
    ln -s /opt/schleuder-cli/bin/schleuder-cli /usr/local/bin/

# entrypoint script
COPY entrypoint.sh /sbin/entrypoint.sh
RUN chmod a+x /sbin/entrypoint.sh

# we need to be able to mount the code into other containers
# like the SMTPD container
# so that schleuder work can be run if needed
VOLUME ["/usr/local/bundle", "/etc/schleuder", "/var/lib/schleuder/lists"]

# final config
EXPOSE 4443
ENTRYPOINT ["/sbin/entrypoint.sh"]
CMD ["schleuder-api-daemon"]

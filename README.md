# [Schleuder](https://git.codecoop.org/schleuder/schleuder3) on `docker`

*Forked from [pudo's schlocker](https://github.com/pudo/schlocker)*

*Then forked from [rysiek's schlocker](https://git.occrp.org/libre/schlocker3) to edit ruby version**

This repo contains `docker` configuration for `schleuder`, an encrypted group email system.

See:

* https://0xacab.org/schleuder/schleuder
* https://0xacab.org/schleuder/schleuder-cli

If a valid database is not found, `schleuder install` is run inside the container to set-up a basic valid database. You can manage the listserver by `docker exec`-ing into the container and using [`schleuder-cli`](https://0xacab.org/schleuder/schleuder-cli) installed within. Or, set-up a [`webschlocker3`](https://git.occrp.org/libre/webschlocker3) for your web interfacing pleasure.

## Running and testing

You need `docker`, obviously. So first, [go and install it](https://docs.docker.com/engine/installation/). Then it's a simple matter of:

```
$ git clone https://0xacab.org/schleuder/schlocker.git
$ docker build --no-cache -t 'schlocker' schlocker/
$ my_api_key=$( head -c 500 /dev/urandom | sha256sum | awk '{print $1}' )
$ docker run --rm --name schlocker --env SCHLOCKER_CONFIG_API_KEYS=$my_api_key -p 127.0.0.1:4443:4443 schlocker
```

To log into the container run in a second terminal:

```
$ docker exec -ti schlocker /bin/bash
```


You can watch the output of `schleuder-api-daemon` in the first terminal, and interface with it using `schleuder-cli` in the second. Or, [you can use the `webschlocker3` image](https://git.occrp.org/libre/webschlocker3/#running-and-testing).

By default `schleuder-api-daemon` listens on port `4443` on all interfaces in the container; you can get the IP of the container with:

```
docker inspect -f '{{.NetworkSettings.IPAddress}}' schlocker-test
```

## Handling mail

Unfortunately, `schleuder` needs to be run directly to ingest new mail (expecting it on `stdin`), and it requires the target list name (including the domain name) as the first argument. This makes dockerizing it a bit more complicated, as to do that, the [MTA](https://en.wikipedia.org/wiki/Message_transfer_agent) that actually receives mail would have to run in the same container. [Not perfect](https://docs.docker.com/engine/userguide/eng-image/dockerfile_best-practices/#run-only-one-process-per-container).

Two possible work-arounds are possible with this image.

### Pipe mail directly into `schleuder` (*EXPERIMENTAL*)

One method is to volume-mount the `/usr/local/bundle` directory *from* container running this image *into* a container running the SMTP daemon, and use `/usr/local/bundle/bin/schleuder` binary to pipe messages to; the full command would be:

```
/usr/local/bundle/bin/schleuder work listname@listserver.example.com
```

This approach also requries volume-mounting the list directory (`$SCHLOCKER_CONFIG_LISTS_DIR`) and the config directory (`$SCHLOCKER_CONFIG_PATH`) to make them available to `schleuder` process run in the SMTP daemon container.

### Queue folder

Alternatively, a "queue folder" approach can be used. Mail is expected to be deposited by the MTA in the `SCHLOCKER_MAILDIR` directory, into [Maildir](https://en.wikipedia.org/wiki/Maildir)-structured subdirectories that match list names. The `entrypoint.sh` script runs `inotifywait` in the background watching these, and when new mail arrives, it's moved to the `SCHLOCKER_TMPDIR` temporary directory's subdirectory matching the list's name, and from there `schleuder` is run with list name gotten from the directory name, and domain name from `SCHLOCKER_HOSTNAME` (this is precisely the reason behind the "only one domain" limitation, by the way).

Hence, `SCHLOCKER_MAILDIR` directory should be bind-mounted into the container, and on the other hand be the place where the MTA drops all mail meant to hit the lists.

## Environment variables

 - `SCHLOCKER_HOMEDIR` (default: `/var/schlocker`)

Path to the home directory of the `$SCHLOCKER_USER` user. By default used as base directory for `SCHLOCKER_MAILDIR`, `SCHLOCKER_CONFIG_LISTS_DIR`, `SCHLOCKER_DB_DATABASE`, `SCHLOCKER_TMPDIR`. Ownership will be automagically set to `$SCHLOCKER_USER:$SCHLOCKER_GROUP`. If the directory doesn't exist, it will be created.

 - `SCHLOCKER_MAILDIR` (default: `$SCHLOCKER_HOMEDIR/mail`)

Path to the Maildir directory being watched for new messages. This directory **should** reside on the same filesystem/volume/mount as the `SCHLOCKER_TMPDIR` directory, as the `entrypoint.sh` script relies on file moves between them being atomic!

If set to a non-empty string, the "queue folder" mail handling method above will be used. If set to an empty string, the "pipe" mail handling method will be used instead (and no `inotify` watches will be set).

 - `SCHLOCKER_USER` (default: `schlocker`)
 - `SCHLOCKER_UID`

The username to run the `schleuder-api-daemon` daemon and `schleuder` mail ingest process as. If the user does not exist, it will be created (with `SCHLOCKER_UID` as uid, if provided; first available `uid` otherwise), and will be put in `SCHLOCKER_GROUP` group.

If both `SCHLOCKER_USER` and `SCHLOCKER_UID` are provided, and there is an existing user with `SCHLOCKER_USER`, but with a different uid, the startup script will fail with an error.

 - `SCHLOCKER_GROUP` (default: `schlocker`)
 - `SCHLOCKER_GID`

The group to run the `schleuder-api-daemon` daemon and `schleuder` mail ingest process as. If the group does not exist, it will be created (with `SCHLOCKER_GID` as uid, if provided; first available `gid` otherwise).

If both `SCHLOCKER_GROUP` and `SCHLOCKER_GID` are provided, and there is an existing user with `SCHLOCKER_GROUP`, but with a different uid, the startup script will fail with an error.

 - `SCHLOCKER_HOSTNAME` (default: hostname of the container)

Hostname/domain name of the mailing list server. Only one hostname/domain name is supported at this time (i.e. you cannot have mailing lists with different domain names on a single `schlocker` instance). This is a `schlocker`'s limitation, not `schleuder`'s!

 - `SCHLOCKER_TMPDIR` (default: `$SCHLOCKER_MAILDIR/.tmp`)

Path to the temporary directory being used for new messages ingestion into `schleuder`. This directory **should** reside on the same filesystem/volume/mount as the `SCHLOCKER_MAILDIR` directory, as the `entrypoint.sh` script relies on file moves between them being atomic!

 - `SCHLOCKER_CONFIG_PATH` (default: `/etc/schleuder/schleuder.yml`)

Location of the config file. Currently this should not be changed from the default.

### Schleuder config variables

These are used to populate the `schleuder.yml` config file.

 - `SCHLOCKER_CONFIG_SUPERADMIN` (default: `root@localhost`)

The superadmin user, able to execute all administrative commands via list commands e-mails and `webschleuder3` (if set-up).

 - `SCHLOCKER_CONFIG_LISTS_DIR` (default: `$SCHLOCKER_HOMEDIR/lists`)

Location of the directory containing `schleuder` lists data.

 - `SCHLOCKER_CONFIG_PLUGINS_DIR` (default: `/etc/schleuder/plugins`)

Plugins directory path.

 - `SCHLOCKER_CONFIG_LOG_LEVEL` (default: `warn`)

Log level. Valid values are (from most to least verbose): `debug`, `info`, `warn`, `error`

 - `SCHLOCKER_CONFIG_SMTP_HOST` (default: `localhost`)

SMTP server host to use when sending mail.

 - `SCHLOCKER_CONFIG_SMTP_PORT` (default: `25`)

SMTP server port used when sending mail.

 - `SCHLOCKER_CONFIG_API_KEYS` (default: autogenerated)

A whitespace-separated list of hexadecimal API keys to be added to the `valid_api_keys` config key. Keys should be at least 64 characters long. If no keys are provided, a key will be autogenerated and added to `valid_api_keys`.

You can retrieve the keys from the config (including the autogenerated one) by running:

```
docker exec <schlocker_container_name> grep -v '#' /etc/schleuder/schleuder.yml | grep -A 10 valid_api_keys
```

### Database settings

These are used to populate the `database` part of the `schleuder.yml` config file. Separate databases are used by `schleuder3` and `webschleuder3`; these settings should thus be different from the ones used for [`webschlocker3`](https://git.occrp.org/libre/webschlocker3/) when used in tandem.

 - `SCHLOCKER_DB_ADAPTER` (default: `sqlite3`)

Database adapter.

 - `SCHLOCKER_DB_DATABASE` (default: `$SCHLOCKER_HOMEDIR/db.sqlite3`)

Database name (or database file path when using `sqlite3` adapter).

 - `SCHLOCKER_DB_ENCODING`

Database encoding (not used for `sqlite3`).

 - `SCHLOCKER_DB_USERNAME`

Database user (not used for `sqlite3`).

 - `SCHLOCKER_DB_PASSWORD`

Database user password (not used for `sqlite3`).

 - `SCHLOCKER_DB_HOST`

Database host (not used for `sqlite3`).

## TODO

* ~~How are we going to have our MTA invoke schleuder? Maybe it should live in the same
  docker instance.~~ *done, inotifywait and some bash magic*
* currently only supports a single listserver domain
* handle failed mail better (move it out of the way, into a separate directory perhaps)
* make the port and interface `schleuder-api-daemon` binds to configurable
* ~~don't run as `root`!~~
